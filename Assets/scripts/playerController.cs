﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour
{
    public float speed;

    private Rigidbody2D rb;
    private Vector2 moveVelocity;
    float lockPos = 0;
    public Vector3 respawnPoint;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
       // respawnPoint = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 moveInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        moveVelocity = moveInput.normalized * speed;
        transform.rotation = Quaternion.Euler(lockPos, lockPos, lockPos);
    }

    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + moveVelocity * Time.fixedDeltaTime);
    }
  // private void OnTriggerEnter2D(Collider2D col)
  // {
  //     if (col.tag == "bunny")
  //     {
  //         transform.position = respawnPoint;
  //     }
  // }
}
