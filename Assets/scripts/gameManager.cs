﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class gameManager : MonoBehaviour
{
    public GameObject player;
    public string gameState = "StartScene";
    public GameObject playerSpawnPoint;
    public int lives = 3;
    public int maxLives = 3;
    public Canvas TitleMenu;
    private ScoreManager theScoreManager;

    private void Start()
    {
        theScoreManager = FindObjectOfType<ScoreManager>();
    }

    public void RestartGame()
    {
        StartCoroutine("RestartGameCo");
    }

    public IEnumerator RestartGameCo()
    {
        theScoreManager.scoreIncreasing = false;
        player.gameObject.SetActive(false);
        yield return new WaitForSeconds(1.0f);
    }

    // Update is called once per frame
    public void Update()
    {
        if (gameState == "StartScreen")
        {
            //StartScreen();
        }
        else if (gameState == "InitializeGame")
        {
            gameState = "SpawnPlayer";
        }
        else if (gameState == "SpawnPlayer")
        {
            //SpawnPlayer();
            if (player != null)
            {
                gameState = "Gameplay";
            }
        }
        else if (gameState == "Gameplay")
        {
            //Gameplay();
            if (player == null)
            {
                gameState = "PlayerDeath";
            }
        }
        else if (gameState == "GameOver")
        {

        }
        else if (gameState == "PlayerDeath")
        {
            // PlayerDeath();

            if (lives < 0)
            {
                if (Input.anyKeyDown)
                {
                    gameState = "SpawnPlayer";
                }
            }
            else
            {
                gameState = "GameOver";

            }
            if (Input.GetKey("escape"))
            {
                Application.Quit();
            }
        }
    }
    }